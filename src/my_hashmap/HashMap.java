package my_hashmap;

public class HashMap {

    private int size = 0;
    private static final int CAPACITY = 16;
    private Node[] table = new Node[CAPACITY];

    public void put(int key, long value) {

        Node node = new Node(key, value);
        int index = node.hashCode() % (CAPACITY - 1);
        Node element = table[index];

        if (element != null) {
            if (element.hashCode() == node.hashCode() && element.equals(node)) {
                element.setValue(node.getValue());
            } else {
                while (element.getNext() != null) {
                    element = element.getNext();
                }
                if (element.hashCode() == node.hashCode() && element.equals(node)) {
                    element.setValue(node.getValue());
                } else {
                    element.setNext(node);
                }
                size++;
            }
        } else {
            table[index] = node;
            size++;
        }
    }

    public long get(int key) {
        Node node = new Node(key, 0);
        int index = node.hashCode() % (CAPACITY - 1);

        Node element = table[index];

        while (element != null) {
            if (element.getKey() == key) {
                return element.getValue();
            }
            element = element.getNext();
        }
        return 0;
    }

    public int size() {
        return size;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Node n : table) {
            if (n != null) {
                sb.append(n).append("\n");
                Node next = n.getNext();
                while (next != null) {
                    sb.append(next).append("\n");
                    next = next.getNext();
                }
            }
        }
        return sb.toString();
    }
}
