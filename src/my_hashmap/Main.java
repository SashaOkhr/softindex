package my_hashmap;

public class Main {

    public static void main(String[] args) {

        HashMap map = new HashMap();
        map.put(4, 23);
        map.put(1, 21);
        map.put(6, 67);
        map.put(12, 23);

        map.put(11, 3);
        map.put(10, 21);
        map.put(9, 67);
        map.put(8, 23);

        map.put(7, 3);
        map.put(5, 67);
        map.put(44, 12);
        map.put(83, 23);

        map.put(34, 3);
        map.put(43, 3);
        map.put(15, 3);
        map.put(51, 3);

        map.put(98, 3);
        map.put(4, 21);
        map.put(98, 22);

        System.out.println(map);

        System.out.println("size = " + map.size());

        System.out.println(map.get(98));
        System.out.println(map.get(4));
        System.out.println(map.get(1));
        System.out.println(map.get(8));
        System.out.println(map.get(83));


    }

}
