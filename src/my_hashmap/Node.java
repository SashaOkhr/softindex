package my_hashmap;

import java.util.Objects;

public class Node {

    private final int key;
    private long value;
    private Node next;

    public Node(int key, long value) {
        this.key = key;
        this.value = value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    public long getValue() {
        return value;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public Node getNext() {
        return next;
    }

    public int getKey() {
        return key;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || obj.getClass() != this.getClass())
            return false;
        Node node = (Node) obj;

        return node.key == key;
    }

    @Override
    public int hashCode() {
        return Objects.hash(key);
    }

    @Override
    public String toString() {
        return "{key=" + key +
                ", value=" + value +
                '}';
    }
}
